<?php

// Подключение к БД и создание PDO объекта
$host = '127.0.0.1';
$db   = 'scotchbox';
$user = 'root';
$pass = 'root';
$charset = 'utf8';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);

// Создание структуры таблицы
createTable($pdo, "domains_clients");

// Наполнение 5 различными записями
insertMainData($pdo, "domains_clients");

// Выборка из таблиц

selectAVGPrice($pdo, 'price', 'domains_clients');
selectCountOfPrices($pdo, 'price', 'domains_clients');
selectSumOfPrices($pdo, 'price', 'domains_clients');
selectMAXPrice($pdo, 'price', 'domains_clients');
selectMINPrice($pdo, 'price', 'domains_clients');

// Добавить колонку
//addColumn($pdo, "domains_clients", "new_column");

// Удалить строку
//deleteClient($pdo, "domains_clients", 1);

// Удалить таблицу
//dropTable($pdo, "domains_clients");

function selectAVGPrice($pdo, $column, $table_name){
    $stmt = $pdo->query("SELECT DISTINCT AVG($column) FROM $table_name");

    while ($row = $stmt->fetch())
    {
        print_r($row["AVG($column)"]."<br>");
    }
}

function selectCountOfPrices($pdo, $column, $table_name){
    $stmt = $pdo->query("SELECT DISTINCT COUNT($column) FROM $table_name");

    while ($row = $stmt->fetch())
    {
        print_r($row["COUNT($column)"]."<br>");
    }
}

function selectSumOfPrices($pdo, $column, $table_name){
    $stmt = $pdo->query("SELECT DISTINCT SUM($column) FROM $table_name");

    while ($row = $stmt->fetch())
    {
        print_r($row["SUM($column)"]."<br>");
    }
}

function selectMAXPrice($pdo, $column, $table_name){
    $stmt = $pdo->query("SELECT DISTINCT MAX($column) FROM $table_name");

    while ($row = $stmt->fetch())
    {
        print_r($row["MAX($column)"]."<br>");
    }
}

function selectMINPrice($pdo, $column, $table_name){
    $stmt = $pdo->query("SELECT DISTINCT MIN($column) FROM $table_name");

    while ($row = $stmt->fetch())
    {
        print_r($row["MIN($column)"]."<br>");
    }
}

function selectDomain($pdo, $column, $table_name, $current){
    $stmt = $pdo->query("SELECT $column FROM $table_name WHERE $column = '$current'");
    while ($row = $stmt->fetch())
    {
        return $row[$column];
    }
}


function createTable($pdo, $table_name){

    $pdo->query("CREATE TABLE IF NOT EXISTS $table_name (
                id int(11) NOT NULL AUTO_INCREMENT,
                name varchar(30) NOT NULL,
                sirname varchar(30) NOT NULL,
                domain varchar(30) NOT NULL,
                price decimal(11) NOT NULL,
                start_date DATE,
                PRIMARY KEY (id)
              ) Engine=InnoDB DEFAULT CHARSET=utf8;");
    }

    // Наполнение 5 различными записями
    function insertMainData($pdo, $table_name){
      insertToTable($pdo, 'Sam', 'Canner', 'www.samcannerhome.com', 1500.50, '2017-04-30', $table_name);
      insertToTable($pdo, 'Nobody', 'Nothing', 'www.nothing.net', 1500.18, '2017-05-11', $table_name);
      insertToTable($pdo, 'John', 'Smith', 'www.ohmyjohn.gb', 1495.35, '2017-05-08', $table_name);
      insertToTable($pdo, 'Mark', 'Dine', 'www.crazygenious.com', 1000, '2017-05-18', $table_name);
      insertToTable($pdo, 'Lol', 'Kek', 'www.lolkek.lolkek', 910.15, '2017-05-28', $table_name);
    }

function insertToTable($pdo, $name, $sirname, $domain, $price, $start_date, $table_name){

    $current_dom = selectDomain($pdo, 'domain', $table_name, $domain);

    if($current_dom != $domain){
      $sql = $pdo->prepare("INSERT INTO $table_name (name, sirname, domain, price, start_date) values ('$name', '$sirname', '$domain', $price, '$start_date');");
      $sql->execute();
    }
}

function addColumn($pdo, $table_name, $column_name){
  $sql = $pdo->prepare("ALTER TABLE $table_name ADD $column_name varchar(30);");
  $sql->execute();
}

function deleteClient($pdo, $table_name, $index){
  $pdo->exec("DELETE FROM $table_name WHERE id = $index");
}

function dropTable($pdo, $table_name){
  $sql = $pdo->prepare("DROP TABLE $table_name;");
  $sql->execute();
}

?>
